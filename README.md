# Javascript Drills



## Getting started
- run `npm install` to install all the dependencies
- run index.js file to get solution of all the problems
- you can also run `npm start` to run the main file
- enter and run `npx jest` command to run test files
